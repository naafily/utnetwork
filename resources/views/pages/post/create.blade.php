@extends('layouts.panel')


@section('panelbread')
    {{ Breadcrumbs::render('post.create',$group ) }}

@endsection


@section('panelhead')
    <h2>
        <div class="wall-tabs">
            <div class="item ossn-wall-container-menu-post" data-name="post"><i class="fa fa-bullhorn"><span>Post</span></i>
            </div>
        </div>
    </h2>

@endsection


@section('panelbody')


    @include('panels.form.create-post')


@endsection













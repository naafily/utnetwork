{{--<div class="ossn-wall-container">--}}
    {{ Form::open(['url' => route('post.store',$group->id), 'class' => 'form-horizontal', 'role' => 'form','files'=>'true','id'=>'post-create' ]) }}


    <div class="ossn-form row {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label('title', 'Title', ['class' => 'col-md-2  col-sm-3 control-label']) !!}

        <div class="col-md-8 col-sm-8">
            {!! Form::text('title', null, ['class' => 'form-control rtl-text' , 'required', 'autofocus']) !!}

            <span class="help-block">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
        </div>
    </div>


    <div class="ossn-form row{{ $errors->has('body') ? ' has-error' : '' }}">
        {!! Form::label('body', 'Body', ['class' => 'col-md-2 col-sm-3 control-label']) !!}

        <div class="col-md-8 col-sm-8">
            {!! Form::textarea('body',  null, ['class' => 'form-control rtl-text', 'required']) !!}

            <span class="help-block">
            <strong>{{ $errors->first('body') }}</strong>
        </span>
        </div>
    </div>

    <div class="ossn-form row {{ $errors->has('vote_type') ? ' has-error' : '' }}">
        <label class="col-md-2 col-sm-3 control-label">Vote Type </label>
        <div class="col-md-8 col-sm-8">
            @include('partials.set-vote-type')
        </div>
        <span class="help-block">
            <strong>{{ $errors->first('vote_type') }}</strong>
    </span>
    </div>


    <div id="poll-container" style="display: none">
        <div class="ossn-form row {{ $errors->has('poll') ? ' has-error' : '' }}">
            <label class="col-md-2 col-sm-3 control-label">Poll</label>
            <div class="col-md-8 col-sm-8">
                @include('panels.form.create-poll')
            </div>
            <span class="help-block">
                <strong>{{ $errors->first('poll') }}</strong>
                </span>
        </div>
    </div>


    <div class="ossn-form row {{ $errors->has('datetime') ? ' has-error' : '' }}">
        <label class="col-md-2 col-sm-3 control-label">Expire at</label>
        <div class="col-md-8 col-sm-8">
            @include('partials.set-expiration')
        </div>
        <span class="help-block">
            <strong>{{ $errors->first('datetime') }}</strong>
        </span>
    </div>


    <div class="ossn-form row ">
        <div class="col-md-10 col-sm-10"></div>
        <div class="col-md-2 col-sm-2     ">
            <button type="submit" class="btn btn-primary">
                Create
            </button>
        </div>
    </div>


    {!! Form::close() !!}


    <div class="ossn-form row ">
        <label class="col-md-2  control-label">Upload Attachment </label>
        <div class="col-md-8">
            @include('panels.form.upload-file')
        </div>
    </div>
{{--</div>--}}

@push('scripts')
    <script src="{{ asset('js/scripts/create-post.js') }}" type="text/javascript"></script>
@endpush

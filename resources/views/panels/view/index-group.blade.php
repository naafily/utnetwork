<div class="table-responsive" style="overflow-x: auto ">

    <table class="table  ">
        <thead>
        <tr>
            <th></th>
            <th>Group Name</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        @forelse ($groups as $group)


            <tr>
                <td>
                    <img
                        src="https://demo.opensource-socialnetwork.org/components/OssnGroups/images/search_group.png?ossn_cache=8ddc3567"
                        width="100" height="100">

                </td>

                <td>
                    <div class="row">
                        <a href="{{ url("/groups/{$group->id}") }}"> {{  Str::limit($group->name )}}</a>
                    </div>
                </td>

                <td>
                    @can( 'update',$group )
                        <a href="{{ url("/groups/{$group->id}/edit") }}" class="btn btn-xs btn-warning">Edit</a>
                    @endcan

                    @can( 'delete',$group )
                        <a href="{{ url("/groups/{$group->id}") }}" data-method="DELETE" data-token="{{ csrf_token() }}"
                           data-confirm="Are you sure?" class="btn btn-xs btn-danger">Delete</a>
                    @endcan

                </td>


            </tr>

        @empty
            <tr>
                <td colspan="2">No groups available.</td>
            </tr>
        @endforelse


        </tbody>


    </table>
</div>


<div class="text-center">

    {!! $groups->appends(['search'=>Request::input('search')])->links() !!}


</div>


{{--<div class="col-md-6">--}}
{{--    <div class="newsfeed-middle ossn-page-contents">--}}
{{--        <div class="ossn-search-page">--}}
{{--            <div class="search-data">--}}
{{--                @forelse ($groups as $group)--}}

{{--                <div class="row">--}}
{{--                    <div class="col-md-2 col-sm-2 col-xs-4">--}}
{{--                        <img--}}
{{--                            src="https://demo.opensource-socialnetwork.org/components/OssnGroups/images/search_group.png?ossn_cache=8ddc3567"--}}
{{--                            width="100" height="100">--}}
{{--                    </div>--}}
{{--                    <div class="col-md-10 col-sm-10 col-xs-8">--}}
{{--                        <div class="group-search-details">--}}
{{--                            <a href="{{ url("/groups/{$group->id}") }}" class="btn btn-xs btn-success">{{  Str::limit($group->name )}}</a>--}}

{{--                            <p class="ossn-group-search-by">By:<a--}}
{{--                                    href="https://demo.opensource-socialnetwork.org/u/administrator"--}}
{{--                                    dideo-checked="true">System Administrator</a></p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endforelse--}}

{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

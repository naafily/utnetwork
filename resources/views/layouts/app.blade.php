<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          href="https://demo.opensource-socialnetwork.org/themes/goblue/images/favicon.ico?ossn_cache=8ddc3567"
          type="image/x-icon">
    <link rel="stylesheet" type="text/css"
          href="https://demo.opensource-socialnetwork.org/cache/css/1633433244/view/bootstrap.min.css">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{asset('css/css-file-icons.css')}}"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans:400italic,700,400">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ossn.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/css/jquery-ui.css">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};


    </script>

    <style>

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }


    </style>

    @stack('styles')
</head>


<body>
@auth
    @include ('partials.sidebar-menu')
@endauth

<div class="ossn-page-loading-annimation" style="display: none;">
    <div class="ossn-page-loading-annimation-inner">
        <div class="ossn-loading"></div>
    </div>
</div>
<div class="ossn-halt ossn-light" style="height: 3925px; display: none;"></div>
<div class="ossn-message-box" style="display: none;">
    <div class="title">
        Add Group
        <div class="close-box" onclick="Ossn.MessageBoxClose();">X</div>
    </div>

    <div class="contents">
        <div class="ossn-box-inner">
            <div style="width:100%;margin:auto;">
                <form action="{{ url('/groups') }}" class="ossn-form ossn-form"
                      method="post" enctype="multipart/form-data">
                    @csrf

                    <fieldset>
                        <div>
                            <label>Group Name</label>
                            <input type="text" name="name">
                            <label>Description</label>
                            <input type="text" name="description">
                            <input type="submit" class="ossn-hidden" id="ossn-group-submit">
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="control">
        <div class="controls">
            <a href="javascript:void(0);" onclick="Ossn.Clk('#ossn-group-submit');" class="btn btn-primary"
               dideo-checked="true">Save</a>
            <a href="javascript:void(0);" onclick="Ossn.MessageBoxClose();" class="btn btn-default"
               dideo-checked="true">Cancel</a>
        </div>
    </div>
</div>
<div class="ossn-viewer" style="display:none"></div>
<div id="app" class="ossn-page-container sidebar-close-page-container">
    <div class="topbar">
        <div class="container-fluid">
            <div class="row">
                @guest

                    <div class="col col-md-2 left-side left">
                    </div>
                    <div class="col-md-7 site-name text-center ">
                        <span><a href="#">UT Network</a></span>
                    </div>
                @endguest
                @auth

                    <div class="col col-md-2">
                        <div class="topbar-menu-left">
                            <li id="sidebar-toggle" data-toggle="1">
                                <a role="button" data-bs-target="#"> <i class="fa fa-th-list"></i></a>
                            </li>
                        </div>
                    </div>
                    <div class="col-md-7 site-name text-center hidden-xs hidden-sm">
                        <span><a href="#">UT Network</a></span>
                    </div>
                    <div class="col col-md-2">
                        <div class="topbar-menu-right">
                            <ul>
                                <li class="ossn-topbar-dropdown-menu">
                                    <div class="dropdown">
                                        <a role="button" data-bs-toggle="dropdown" data-bs-target="#"
                                           dideo-checked="true"
                                           aria-expanded="false"
                                           class=""><i class="fa fa-sort-down"></i></a>
                                        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu"
                                            style="">
                                            <li><a class="dropdown-item menu-topbar-dropdown-account_settings"
                                                   href="{{ url('users/'.Auth::user()->id)."/edit" }}"
                                                   dideo-checked="true">Edit
                                                    profile</a></li>


                                            <li><a class="dropdown-item menu-topbar-dropdown-logout"
                                                   href="{{ route('logout') }}"
                                                   dideo-checked="true">Log out</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="ossn-notif-notification">
                                    <a href="javascript:void(0);" class="ossn-notifications-notification"
                                       onclick="Ossn.NotificationShow(this)" dideo-checked="true">
                                        <span>
                                            <span class="ossn-notification-container hidden" style="display: none;"></span>
                                            <div class="ossn-icon ossn-icons-topbar-notification"><i class="fa fa-globe-americas"></i></div>
                                        </span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>

                @endauth
            </div>
        </div>
    </div>

    <div id='center' class="main center">
        <div class="container">
            <div class="row">
                @include('flash::message')
            </div>


        </div>


        @yield('content')


    </div>


</div>


<!-- Scripts -->


</body>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('js/arrive.min.js')}}"></script>
<script src="{{ asset('js/scripts/user-avatar.js') }}"></script>
<script src="{{ asset('js/scripts/rtl.js') }}"></script>
<script src="{{ asset('js/scripts/edit-member.js') }}"></script>

<script async="" src="//www.google-analytics.com/analytics.js"></script>
<script src="https://demo.opensource-socialnetwork.org/cache/js/1633433244/view/ossn.en.language.js"></script>
<script src="https://demo.opensource-socialnetwork.org/vendors/jquery/jquery-3.6.0.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
{{--<script src="https://demo.opensource-socialnetwork.org/vendors/jquery/jquery-arhandler-1.1-min.js"></script>--}}
<script
    src="https://demo.opensource-socialnetwork.org/components/OssnAutoPagt ination/vendors/jquery.scrolling.js?ossn_cache=8ddc3567"></script>
<script src="//cdn.jsdelivr.net/places.js/1/places.min.js"></script>
<script src="https://demo.opensource-socialnetwork.org/vendors/jquery/jquery.tokeninput.js"></script>
<script src="{{ asset('js/ossn.js') }}"></script>
<script src="https://demo.opensource-socialnetwork.org/cache/js/1633433244/view/ossn.site.public.js"></script>
<script src="{{ asset('js/ossn.site.js') }}"></script>
<script src="https://demo.opensource-socialnetwork.org/cache/js/1633433244/view/ossn.chat.js"></script>
<script src="https://demo.opensource-socialnetwork.org/themes/goblue/vendors/bootstrap/js/bootstrap.min.js?v5"></script>
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
<script src="{{ asset('js/scripts/edit-member.js') }}"></script>



{{--<script src="{{ asset('js/ossn.site.public.js') }}"></script>--}}
{{--@stack('scripts')--}}


</html>

<div class="post-menu">
    <div class="dropdown">
        <a role="button" data-bs-toggle="dropdown" class="btn btn-link show" data-bs-target="#" dideo-checked="true"
           aria-expanded="false">
            <i class="fa fa fa-ellipsis-h"></i>
        </a>
        <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu" style="">

            {!! Form::open(['method'=>'get','url' =>"/posts/$post->id/edit" ,'class'=> "form-horizontal",'role'=>'form' ,'name'=>'edit-group' ]) !!}
            <input type="submit" class="dropdown-item" value="Edit">
            {!! Form::close( ) !!}

            <a class="dropdown-item" href="{{ url("/posts/{$post->id}") }}" data-method="DELETE"
               data-token="{{ csrf_token() }}" data-confirm="Are you sure?">Delete</a>
        </ul>

    </div>
</div>



<div class="sidebar sidebar-close sidebar-open" style="height: 6144px;">
    <div class="sidebar-contents">
        <div class="newseed-uinfo">
            <img
                src="https://demo.opensource-socialnetwork.org/avatar/administrator/small/55e85d198707ffa032434cc5d2839a4b.jpeg?ossn_cache=8ddc3567">

            <div class="name">
                <span
                    href="https://demo.opensource-socialnetwork.org/u/administrator">{{ ucfirst(Auth::user()->first_name[0])}} {{ucfirst(Auth::user()->last_name[0])}}</span>
                <a class="edit-profile" href="{{ url('users/'.Auth::user()->id)."/edit" }}">
                    Edit Profile</a>
            </div>
        </div>
        <div class="sidebar-menu-nav">
            <div class="sidebar-menu">
                <ul id="menu-content" class="menu-content collapse out">
                    <li>
                        <a href="{{ url('/home') }}"> <i class="fa fa-home fa-lg"></i>
                            Home</a>
                    </li>

                    @can('viewAny','App\Models\Post')
                        <li>
                            <a href="{{ url('/posts') }}">
                                <i class="fa fa-newspaper-o fa-lg"></i> Posts
                            </a>
                        </li>
                    @endcan

                    @can('viewAny','App\Models\Group')
                        <li data-bs-toggle="collapse" data-bs-target="#nm1471e4e05a4db95d353cc867fe317314"
                            class="menu-section-groups active" aria-expanded="true">
                            <a href="javascript:void(0);" dideo-checked="true">
                                <i class="fa fa-users fa-lg"></i>Groups
                                <span class="arrow"></span>
                            </a>
                        </li>

                        <ul class="sub-menu collapse" id="nm1471e4e05a4db95d353cc867fe317314" style="">
                            <a id="ossn-group-add" class=menu-section-item-a-addgroup"
                               href="javascript:void(0);"
                               dideo-checked="true">
                                <li class="menu-section-item-addgroup">New Group</li>
                            </a>
                            <a class="menu-section-item-a-allgroups"
                               href="{{ url('/groups') }}"
                               dideo-checked="true">
                                <li class="menu-section-item-allgroups">Groups</li>
                            </a>
                        </ul>

                    @endcan

                    @can('viewAny','App\User')
                    @endcan
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); if(confirm('Are you sure you want to log out?')){document.getElementById('logout-form').submit();} ">
                            <i class="fa fa-sign-out fa-lg"></i>
                            Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>


    </div>
</div>



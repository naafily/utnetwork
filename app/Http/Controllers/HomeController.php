<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {

        if (auth()->user()->isSuper()) {
            $posts = Post::
            active()
                ->select('posts.*')
                ->ordered()
                ->paginate(10);

        } else {
            $user = auth()->user();
            error_log("----in query----");
            error_log($user->last_login2);
            error_log($user->last_login);
            error_log("----in query---");
            $posts = Post::join('group_user', function ($q) {
                $q->on('group_user.group_id', '=', 'posts.group_id')
                    ->where('group_user.user_id', '=', auth()->user()->id)
                    ->where('posts.created_at', '>', auth()->user()->last_login);

            })
                ->active()
                ->select('posts.*')
                ->ordered()
                ->paginate(10);
            $user = auth()->user();
            error_log("--------");
            error_log($user->last_login2);
            error_log($user->last_login);
            error_log("-------");
            $user->last_login = $user->last_login2;
            $user->save();
            return view('pages.home', compact('posts'));

        }
    }
}
